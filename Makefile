
HOSTNAME_FQDN := codeberg-test.org

export BUILDDIR := /tmp/build
export GOROOT := ${BUILDDIR}/go
export GOPATH := ${BUILDDIR}/gitea
export PATH := ${GOROOT}/bin:${GOPATH}/bin:${PATH}

GOTAR = go1.14.13.$(shell uname | tr [:upper:] [:lower:])-amd64.tar.gz

TARGETS = ${GOPATH}/bin/reg-server

all : ${TARGETS}

${GOPATH}/bin/reg-server : main.go ${GOROOT}/bin/go
	go build -o $@ $<

${GOROOT}/bin/go :
	mkdir -p ${GOROOT}/Downloads
	wget -c --no-verbose --directory-prefix=${GOROOT}/Downloads https://dl.google.com/go/${GOTAR}
	tar xfz ${GOROOT}/Downloads/${GOTAR} -C ${BUILDDIR}

deployment : deploy-reg-server

deploy-reg-server : ${GOPATH}/bin/reg-server
	-ssh root@${HOSTNAME_FQDN} systemctl stop reg-server
	rsync -av -e ssh --chown=root:root $< root@${HOSTNAME_FQDN}:/usr/local/bin/
	rsync -av -e ssh --chown=root:root etc root@${HOSTNAME_FQDN}:/
	ssh root@${HOSTNAME_FQDN} systemctl daemon-reload
	ssh root@${HOSTNAME_FQDN} systemctl enable reg-server
	ssh root@${HOSTNAME_FQDN} systemctl start reg-server
	ssh root@${HOSTNAME_FQDN} systemctl status reg-server

clean :
	${MAKE} -C ${GOPATH}/src/code.gitea.io/gitea clean

realclean :
	rm -rf ${BUILDDIR}

