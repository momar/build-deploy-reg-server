module codeberg.org/codeberg/member-registration

go 1.14

require (
	github.com/snapcore/go-gettext v0.0.0-20201130093759-38740d1bd3d2
	github.com/yuin/goldmark v1.3.2
	golang.org/x/text v0.3.5
)
